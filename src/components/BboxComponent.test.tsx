import * as React from 'react';
import { render, fireEvent } from '@testing-library/react';
import BboxComponent from './BboxComponent';

describe('src/components/BboxComponent', () => {
  const props = {
    onClickButton1: jest.fn(),
    onClickButton2: jest.fn(),
    currentBbox: {
      left: -6870440.749573884,
      bottom: 5623435.0799058415,
      right: 7997443.616050693,
      top: 11525334.644193,
    },
  };

  it('should call the corresponding function when clicking the buttons', () => {
    const { getAllByRole } = render(<BboxComponent {...props} />);
    const buttonList = getAllByRole('button');

    fireEvent.click(buttonList[0]);
    expect(props.onClickButton1).toHaveBeenCalledTimes(1);

    fireEvent.click(buttonList[1]);
    expect(props.onClickButton2).toHaveBeenCalledTimes(1);
  });

  it('should show the given Bbox', () => {
    const { container } = render(<BboxComponent {...props} />);
    const bboxText = container.querySelector('p').textContent;

    expect(bboxText).toContain(
      `Left: ${props.currentBbox.left}` &&
        `Right: ${props.currentBbox.right}` &&
        `Top: ${props.currentBbox.top}` &&
        `Bottom: ${props.currentBbox.bottom}`,
    );
  });
});
