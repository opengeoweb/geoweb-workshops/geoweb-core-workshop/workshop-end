import * as React from 'react';
import { Paper, Typography, ButtonGroup, Button } from '@material-ui/core';

interface BboxComponentProps {
  currentBbox: { left: number; right: number; bottom: number; top: number };
  onClickButton1: () => void;
  onClickButton2: () => void;
}

const BboxComponent: React.FC<BboxComponentProps> = ({
  currentBbox,
  onClickButton1,
  onClickButton2,
}: BboxComponentProps) => {
  return (
    <Paper style={{ padding: '10px', width: '100%' }}>
      <ButtonGroup>
        <Button variant="contained" onClick={onClickButton1}>
          N-Europe
        </Button>
        <Button variant="contained" onClick={onClickButton2}>
          Netherlands
        </Button>
      </ButtonGroup>
      <Typography variant="subtitle1">Current Bbox:</Typography>
      <Typography variant="body2">
        Left: {currentBbox.left}
        <br /> Bottom: {currentBbox.bottom}
        <br /> Right: {currentBbox.right}
        <br /> Top: {currentBbox.top}
        <br />
      </Typography>
    </Paper>
  );
};

export default BboxComponent;
