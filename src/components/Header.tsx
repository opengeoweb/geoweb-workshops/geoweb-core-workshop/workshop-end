/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React from 'react';
import { Grid } from '@material-ui/core';
import { InfoDialog } from './InfoDialog';

const GeoWebLogo = './geowebLogo.svg';

const Header: React.FC = () => (
  <div>
    <Grid container>
      <Grid item xs={1}>
        <img alt="Home of GeoWeb" style={{ height: '30px' }} src={GeoWebLogo} />
      </Grid>
      <Grid item xs={10}>
        <span style={{ height: '30px' }}>GeoWeb Workshop: 8th of May 2020</span>
      </Grid>
      <Grid item xs={1}>
        <Grid container justify="flex-end">
          <InfoDialog />
        </Grid>
      </Grid>
    </Grid>
  </div>
);

export default Header;
