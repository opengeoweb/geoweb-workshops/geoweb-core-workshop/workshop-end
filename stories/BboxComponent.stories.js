/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { storiesOf } from '@storybook/react';
import React from 'react';

import BboxComponent from '../src/components/BboxComponent';

const northernEuropeBbox = {
  srs: 'EPSG:3857',
  bbox: {
    left: -6870440.749573884,
    bottom: 5623435.0799058415,
    right: 7997443.616050693,
    top: 11525334.644193,
  },
};

storiesOf('GeoWeb Workshop', module).add('Bbox Component', () => (
  <div style={{ height: '100%' }}>
    <BboxComponent
      currentBbox={northernEuropeBbox.bbox}
      onClickButton1={() => {
        // eslint-disable-next-line no-alert
        alert('Button 1');
      }}
      onClickButton2={() => {
        // eslint-disable-next-line no-alert
        alert('Button 2');
      }}
    />
  </div>
));
