/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * */

/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { storiesOf } from '@storybook/react';
import React from 'react';
import App from '../src/App';

storiesOf('GeoWeb Workshop', module).add('Viewer', () => (
  <div style={{ height: '100%' }}>
    <App />
  </div>
));
